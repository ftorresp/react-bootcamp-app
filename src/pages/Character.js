import { useEffect, useState } from "react"
import { Link, useParams } from "react-router-dom"
import Layout from "../component/Layout"
import { Title } from "../component/Title"
import { getCharacterById } from "../services/api"


const Character = () => {

  const [ character, setCharacter ] = useState({})
  const { id } = useParams()

  useEffect(() => {
    getCharacterById(id)
      .then(resp => resp.json())
      .then(data => setCharacter(data))
  }, [id])


  const { name, image } = character

  if (!character.id) return 'cargando'

  return (
    
    <Layout>

      <Title>{ name }</Title>

      <main>

        <section className="characters-list">
          <div className="character-list__container">
            
            <article className="character-card">
              <img 
                src={ image } 
                alt={ name }
                className="character-card__image"
              />
              <h2 className="character-card__title">{ name }</h2>
              
            </article>

            <Link to='/'>Inicio</Link>
          
          </div>
        </section>
      </main>
    
    </Layout>
  )
}

export default Character