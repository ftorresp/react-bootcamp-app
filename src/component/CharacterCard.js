import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

import './CharacterCard.css'

const styles = {
  link: {
    display: 'inline-block',
    padding: '16px',
    backgroundColor: 'gray'
  },
  image: {
    border: '2px yellow solid'
  }
}

const CharacterCard = ({ id, name, image }) => {
  return (
    <article className="character-card">
      <img 
        src={image} 
        alt={name}
        className="character-card__image"
        style={styles.image}
      />
      <h2 className="character-card__title">{ name }</h2>
      <Link 
        to={`/character/${id}`}
        className="character-card__link"
        style={{display: 'inline-block', padding: '16px', backgroundColor: 'gray'}}
      >
        Ver personaje
      </Link>
    </article>
  )
}

CharacterCard.propTypes = {
  id: PropTypes.number,
  name: PropTypes.string,
  image: PropTypes.string
}

export default CharacterCard