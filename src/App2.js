import { Component } from 'react';
import CharacterCard from './component/CharacterCard';
import Layout from './component/Layout';
import { Title } from './component/Title';

import './App.css'
import Modal from './component/Modal';

class App extends Component {

  render () {
    return (
      <div>
        <Layout>
          <button
            onClick={() => this.handleClick()}
          >
            Mostrar
          </button>
          <button onClick={() => this.increase()} >+1</button>
          <br/>

          { this.state.isShown && <Modal counter={this.state.counter}/> }

          <Title>Personajes</Title>

          <section className="characters-list">
            <div className="character-list__container">

              {
                !this.state.characters.length
                ?
                  'Cargando'
                :
                  this.state.characters.map(character => <CharacterCard key={character.name} {...character} />)
              }

            </div>
          </section>
        
        </Layout>
      </div>
    )
  }
}

export default App;
