import { Component } from 'react';
import CharacterCard from '../component/CharacterCard';
import Layout from '../component/Layout';
import { Title } from '../component/Title';

class Home extends Component {

  constructor () {
    super()
    this.state = {
      characters: []
    }
  }

  componentDidMount() {
    fetch('https://rickandmortyapi.com/api/character')
      .then(resp => resp.json())
      .then(data => this.setState({
        ...this.state,
        characters: data.results
      }))
      .catch(err => {
        console.log(err)
      })
  }

  render () {
    return (
      <Layout>

        <Title>Personajes</Title>

        <section className="characters-list">
          <div className="character-list__container">

            {
              !this.state.characters.length
              ?
                'Cargando'
              :
                this.state.characters.map(character => <CharacterCard key={character.name} {...character} />)
            }

          </div>
        </section>
      
      </Layout>
    )
  }
}

export default Home;
