const url = 'https://rickandmortyapi.com/api/character'

export const getCharacters = () => {
  return fetch(url)
}

export const getCharacterById = (id) => {
  return fetch(`${url}/${id}`)
}