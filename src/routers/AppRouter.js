import { BrowserRouter, Route, Routes } from "react-router-dom"
import Home from "../pages/Home"
import Character from "../pages/Character"


const AppRouter = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' element={ <Home /> } />

        <Route path='/character/:id' element={ <Character /> } />

        <Route path='*' element={ 'Error 404' } />
      </Routes>
    </BrowserRouter>
  )
}

export default AppRouter