import { Footer } from "./Footer"
import Header from "./Header"

const Layout = ({children}) => {
  return (
    <>
      <div className="layout">
        <div className="layout__container">
          <Header />
          { children }
        </div>
        <Footer />
      </div>
    </>
  )
}

export default Layout