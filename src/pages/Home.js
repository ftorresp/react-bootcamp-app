import { useEffect, useState } from 'react';
import CharacterCard from '../component/CharacterCard';
import Layout from '../component/Layout';
import { Title } from '../component/Title';
import { getCharacters } from '../services/api';

const Home = () => {

  const [ characters, setCharacters ] = useState([])

  useEffect(() => {
    getCharacters()
      .then(resp => resp.json())
      .then(data => setCharacters(data.results))
      .catch(err => {
        console.log(err)
      })
  }, [])

  return (
    <Layout>

      <Title>Personajes</Title>

      <section className="characters-list">
        <div className="character-list__container">

          {
            !characters.length
            ?
              'Cargando'
            :
              characters.map(character => <CharacterCard key={character.name} {...character} />)
          }

        </div>
      </section>
    
    </Layout>
  )
  
}

export default Home;
