import { Component } from "react"
import { Link } from "react-router-dom"
import Layout from "../component/Layout"
import { Title } from "../component/Title"


class Character extends Component {

  constructor () {
    super()
    this.state = {
      character: {}
    }
  }

  componentDidMount () {
    const [,,id] = window.location.pathname.split('/')
    fetch(`https://rickandmortyapi.com/api/character/${id}`)
      .then(resp => resp.json())
      .then(data => this.setState({
        ...this.state,
        character: data
      }))
  }

  render () {

    const { name, image } = this.state.character

    return (
      
      <Layout>

        <Title>{ name }</Title>

        <main>

          <section className="characters-list">
            <div className="character-list__container">
              
              <article className="character-card">
                <img 
                  src={ image } 
                  alt={ name }
                  className="character-card__image"
                />
                <h2 className="character-card__title">{ name }</h2>
               
              </article>

              <Link to='/'>Inicio</Link>
            
            </div>
          </section>
        </main>
      
      </Layout>
    )
  }
}

export default Character